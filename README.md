## Abstract
---

Priority Queue is one of the most efficient abstract data type which associates each element with a "priority" element. Based on this priority, elements can be operated upon like a stack or a queue with top most priority element being served first. 
This repository is an implementation of Priority queue using the concepts of heap to add/remove/peek elements from the abstract data structure. This implementation is then further improved by adding **Strategy design pattern** and **Iterator pattern** to efficiently handle and iterate over generic data types which needs to be contained in our queue. Strategy pattern is specifically used in this context to handle various requests of same operations but different parameters( comparators in this case).
Upon complete and efficient implementation of the priority queue, this queue implementation is then used to demonstrate **Command design pattern** to perform undo/redo operations on a priority queue. 


## Command pattern usage 
--- 
To use the command pattern: 

1. Import the priorityQueue.PriorityHeap library that is created in this project. 
2. Initialize the CommandInvoker object as: 
```PriorityHeap<String> queue = new PriorityHeap();```
3. Create CommandInvoker object to execute ICommand objects as:
```CommandInvoker<String> invoker = new CommandInvoker(queue);```
4. Use the Add/Remove child classes of ICommand interface to make new command objects as:
```AddElementes queueAdd = new AddElements(getRandomString());```
5. Use the Invoker object to execute commands on queue as:
```invoker.execute(queueAdd);```
6. Use Invoker object to undo/redo last executed commands as:
```invoker.undoCommand();```

