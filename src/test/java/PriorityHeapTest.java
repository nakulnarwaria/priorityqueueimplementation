
import java.util.*;
import org.junit.jupiter.api.Test;
import priorityQueue.PriorityHeap;

import static org.junit.Assert.*;


public class PriorityHeapTest {

	/*
	 * Testing Student class 
	 */
	@Test()
    void testUpperRangeGPA(){
	    try{
            Student student = new Student();
            student.setGpa(10.0);
            fail();
        }catch (Exception e){
            String actualMessage = ApplicationConstants.INVALID_INPUT + " " + ApplicationConstants.GPA_RANGE_MSG;
            assertEquals(e.getMessage(),actualMessage);
        }
    }
	 
	@Test()
    void testUpperRangeNumberOfUnits(){
	    try{
            Student student = new Student();
            student.setNumberOfUnits(300);
            fail();
        }catch (Exception e){
            String actualMessage = ApplicationConstants.INVALID_INPUT + " " + ApplicationConstants.UNITS_RANGE_MSG;
        assertEquals(e.getMessage(),actualMessage);
        }
	}

	@Test()
	void testLowerRangeGPA(){
	    try{
	        Student student = new Student();
	        student.setGpa(-4.0);
	        fail();
	    }catch (Exception e){
	        String actualMessage = ApplicationConstants.INVALID_INPUT + " " + ApplicationConstants.GPA_RANGE_MSG;
	        assertEquals(e.getMessage(),actualMessage);
	    }
	}	

	    
	@Test()
	void testLowerRangeNumberOfUnits(){
	    try{
	        Student student = new Student();
	        student.setNumberOfUnits(-150);
	        fail();
	    }catch (Exception e){
	        String actualMessage = ApplicationConstants.INVALID_INPUT + " " + ApplicationConstants.UNITS_RANGE_MSG;
	        assertEquals(e.getMessage(),actualMessage);
	    }
	}
	
	/*
	 *  Testing heap edge cases against actual priority heap implementation.
	 */
	@Test
    void testAddEqualPriorityStudent() throws Exception{
        PriorityHeap<Student> list = new PriorityHeap();
        Queue<Student> expectedList = new java.util.PriorityQueue<>();

        Student student = createRandomNewStudent();
        
        list.add(student);
        expectedList.add(student);

        assertEquals(expectedList.peek().getRedId(),list.peek().getRedId());

        expectedList.add(student);
        list.add(student);
        assertEquals(expectedList.peek().getRedId(),list.peek().getRedId());
    }
	
	
	@Test()
	void testDisplay() {

		PriorityHeap<Student> studentsList = new PriorityHeap<Student>();
	    Queue<Student> expectedStudentList = new java.util.PriorityQueue<Student>();
	    
	    for(int i=0;i<10;i++) {
	    	Student student = createRandomNewStudent();
	    	studentsList.add(student);
	    	expectedStudentList.add(student);
	    }
	    
	    List<Student> sortedStudentListActual = studentsList.getPrioritySortedList();
		
	    assertEquals(expectedStudentList.size(),sortedStudentListActual.size());
	
	    int i=0;
	    while(!expectedStudentList.isEmpty()) {
	        assertEquals(expectedStudentList.poll().getRedId(), sortedStudentListActual.get(i).getRedId());
	        i++;
	    }

	}
	
	/*
	 * Test the peek functionality of the heap when heap is empty
	 */
	@Test()
	void testEmptyHeapPeek() {
		PriorityHeap queue = new PriorityHeap();
		assertNull(queue.peek());
	}

	/*
	 * Tests the peek functionality of heap when heap is not empty 
	 */
	@Test()
	void testPeek() throws Exception {
        
		PriorityHeap<Student> studentsList = new PriorityHeap<Student>();
	    Queue<Student> expectedStudentList = new java.util.PriorityQueue<Student>();
	    
	    for(int i=0;i<10;i++) {
	    	Student student = createRandomNewStudent();
	    	studentsList.add(student);
	    	expectedStudentList.add(student);
	    }
	  
	    assertEquals(studentsList.peek(),expectedStudentList.peek());
	    
	}
	
	/*
	 * Tests the remove functionality of the heap when the heap is empty 
	 */
	@Test()
	void testEmptyHeapPoll() {
		
		PriorityHeap queue = new PriorityHeap();
		assertNull(queue.poll());

	}
	
	/*
	 * Tests the remove functionality of the heap when heap is not empty 
	 */
	@Test()
	void testPoll(){
	   
		PriorityHeap<Student> studentsList = new PriorityHeap<Student>(); 
	    Queue<Student> expectedStudentList = new java.util.PriorityQueue<Student>();
	    
	    for(int i=0;i<10;i++) {
	    	Student student = createRandomNewStudent();
	    	studentsList.add(student);
	    	expectedStudentList.add(student);
	    }

	    for(int i=0;i<10;i++)
	    	assertEquals(studentsList.poll(),expectedStudentList.poll());
	    
	}
	
	@Test()
	void testToArray() {
		PriorityHeap<Student> list = new PriorityHeap<Student>();
	    Queue<Student> expectedList = new java.util.PriorityQueue<Student>();
	    
	    Student randomStudent = createRandomNewStudent();
	    for(int i=0;i<10;i++) {
	    	list.add(randomStudent);
	    	expectedList.add(randomStudent);
	    	randomStudent = createRandomNewStudent();
	    }
	    
	    Object[] listElements = list.toArray();
	    Object[] expectedListElements = expectedList.toArray(); 
	    
	    for (int i=0; i<listElements.length; i++){
            assertEquals((Student)listElements[i],(Student) expectedListElements[i]);
        }
	    
	}
	
	@Test()
	void testToString() {
		PriorityHeap<Student> list = new PriorityHeap<Student>();
	    Queue<Student> expectedList = new java.util.PriorityQueue<Student>();
	    
	    Student randomStudent = createRandomNewStudent();
	    for(int i=0;i<10;i++) {
	    	list.add(randomStudent);
	    	expectedList.add(randomStudent);
	    	randomStudent = createRandomNewStudent();
	    }
	    
        assertEquals(expectedList.toString(),list.toString());
        
	}
	
	@Test
    void testToArrayWithParam(){
        PriorityHeap<Student> list = new PriorityHeap();
        Queue<Student> expectedList = new java.util.PriorityQueue<>();

        Student randomStudent = createRandomNewStudent();
	    for(int i=0;i<10;i++) {
	    	list.add(randomStudent);
	    	expectedList.add(randomStudent);
	    	randomStudent = createRandomNewStudent();
	    }

        Student[] listElements = list.toArray(new Student[5]);
        Student[] expectedListElements = expectedList.toArray(new Student[5]);

        for (int i=0;i<5; i++)
            assertEquals(listElements[i].toString(), expectedListElements[i].toString());
     
    }
	
	@SuppressWarnings("restriction")
	@Test()
	void testRemove() {
		PriorityHeap<Student> list = new PriorityHeap<Student>();
	    Queue<Student> expectedList = new java.util.PriorityQueue<Student>();
	    
	    Student randomStudent = createRandomNewStudent();
	    for(int i=0;i<10;i++) {
	    	list.add(randomStudent);
	    	expectedList.add(randomStudent);
	    	randomStudent = createRandomNewStudent();
	    }
	    
	    list.remove(randomStudent);
	    expectedList.remove(randomStudent);
	  
	    while(!expectedList.isEmpty() && !list.isEmpty()) 
	    	assertEquals(expectedList.poll(),list.poll());   	
	}
	
	@Test()
	void testEmptyHeapRemove() {
		
		PriorityHeap<Student> list = new PriorityHeap<Student>();
	    Queue<Student> expectedList = new java.util.PriorityQueue<Student>();	    
	    Student randomStudent = createRandomNewStudent(); 
    	assertEquals(list.remove(randomStudent),expectedList.remove(randomStudent));   	
	}
	
	@Test()
	void testNonExistantElementRemove() {
		PriorityHeap<Student> list = new PriorityHeap<Student>();
	    Queue<Student> expectedList = new java.util.PriorityQueue<Student>();
	    
	    Student randomStudent = createRandomNewStudent();
	    for(int i=0;i<10;i++) {
	    	list.add(randomStudent);
	    	expectedList.add(randomStudent);
	    	randomStudent = createRandomNewStudent();
	    }
	    
	    randomStudent = createRandomNewStudent();
	    assertEquals(list.remove(randomStudent),expectedList.remove(randomStudent)); 
	    
	}

	@Test
	void testListParameterConstructor(){
		List<Integer> expectedList = new ArrayList<>();
		for (int i=0; i< 10; i++){
			expectedList.add(getRandomNumber());
		}

		PriorityHeap<Integer> list = new PriorityHeap<>(expectedList);
		Queue<Integer> expectedQueue = new java.util.PriorityQueue<>(expectedList);

		assertEquals(expectedQueue.toString(), list.toString());
	}

	@Test
	void testSortedSetConstructorParameter(){
		SortedSet<Integer> set = new TreeSet<>();
		Collection<Integer> collection = new TreeSet<>();

		for (int i=0; i< 10; i++){
			set.add(getRandomNumber());
			collection.add(getRandomNumber());
		}

		PriorityHeap<Integer> list = new PriorityHeap<>(set);
		Queue<Integer> expectedList = new java.util.PriorityQueue<>(set);

		assertEquals(expectedList.toString(), list.toString());

		list = new PriorityHeap<>(collection);
		expectedList = new java.util.PriorityQueue<>(collection);

		assertEquals(expectedList.toString(), list.toString());
	}

	@Test
	void testPriorityQueueConstructorParameter(){
		Queue<Integer> collection = new PriorityQueue<>();
		PriorityQueue<Integer> queue = new PriorityQueue<>();
		for (int i=0; i< 10; i++){
			collection.add(getRandomNumber());
			queue.add(getRandomNumber());
		}

		PriorityHeap<Integer> list = new PriorityHeap<>(collection);
		Queue<Integer> expectedList = new java.util.PriorityQueue<>(collection);

		assertEquals(expectedList.toString(), list.toString());

		list = new PriorityHeap<>(queue);
		expectedList = new java.util.PriorityQueue<>(queue);

		assertEquals(expectedList.toString(), list.toString());
	}
	
	@Test()
	void testCustomComparatorQueues() {
		Comparator<Integer> reverse = (Integer s1,Integer s2) -> {
			return Double.compare(s2, s1);
		};
		
		PriorityHeap<Integer> list = new PriorityHeap<Integer>(reverse);
	    Queue<Integer> expectedList = new java.util.PriorityQueue<Integer>(reverse);
	    
	    for(int i=0;i<10;i++) {
	    	int ran = getRandomNumber();
	    	list.add(ran);
	    	expectedList.add(ran);
	    }

	    while(!expectedList.isEmpty()) {
	    	assertEquals(expectedList.poll(),list.poll());
	    }
	}
	
	@Test
	void testQueueWithoutComparators() {
		PriorityHeap<Object> list = new PriorityHeap<Object>();
	    Queue<Object> expectedList = new java.util.PriorityQueue<Object>();
	    Object randomObject = new Object();
	    String listReturnVal;
	    String expectedListReturnVal;
	    try {
	    	list.add(randomObject);
	    	listReturnVal = "Value added!";
	    }
	    catch(Exception e) {
	    	listReturnVal = e.toString();
	    }
	    try {
	    	expectedList.add(randomObject);
	    	expectedListReturnVal = "Value added!";
	    }
	    catch(Exception e) {
	    	expectedListReturnVal = e.toString();
	    }
	    assertEquals(listReturnVal, expectedListReturnVal);
	}
	
	/*
	 * Test Iterator for queue implementation 
	 */
	
	@Test
    void testIterator(){

        PriorityHeap<Student> list = new PriorityHeap<>();
        Queue<Student> expectedList = new java.util.PriorityQueue<>();

        int i = 10;
        while (i > 0) {
            list.add(createRandomNewStudent());
            i--;
        }

        for (Student student : list) {
        	expectedList.add(student);
            assertEquals(expectedList.peek().getRedId(), list.peek().getRedId());
        }
    }

    @Test
    void testIteratorConcurrentModification() {
        PriorityHeap<Student> list = new PriorityHeap<>();

        int counter = 10;
        String returnVal = "Use of iterator.";
        while (counter > 0){
            Student student = createRandomNewStudent();
            list.add(student);
            counter--;
        }
        
        
        try {
            for (Student student: list) {
                list.poll();
                Student randomStudent = createRandomNewStudent();
                list.add(randomStudent);
                list.peek();
                list.remove(randomStudent);
                returnVal = "Operations performed while using iterator.";
            }

        } 
        catch (ConcurrentModificationException e){
        	returnVal = "ConcurrentModificationException";
        }
        catch (Exception e)
        {
        	returnVal = "Unexpected error occured.";
        }
        
        
        assertEquals(returnVal,"ConcurrentModificationException");

    }

    
    @Test
    void testIteratorOnEmptyHeap(){
    	PriorityHeap<Integer> list = new PriorityHeap<Integer>();
	    Queue<Integer> expectedList = new java.util.PriorityQueue<Integer>();
      
        Iterator listIterator = list.iterator();
        Iterator expectedListIterator = expectedList.iterator();
        
        String listReturnVal;
	    String expectedListReturnVal;
	    try {
	    	listIterator.next();
	    	listReturnVal = "Moved to next Element";
	    }
	    catch(Exception e) {
	    	listReturnVal = e.toString();
	    }
	    try {
	    	expectedListIterator.next();
	    	expectedListReturnVal = "Moved to next Element";
	    }
	    catch(Exception e) {
	    	expectedListReturnVal = e.toString();
	    }
	    assertEquals(listReturnVal, expectedListReturnVal);
        
    }

    
	/*
	 * Helper methods for JUnit cases
	 */
	private Student createRandomNewStudent(){
	    Student student = new Student();
	
	    try{
	        student.setName(getRandomString());
	        student.setRedId(getRandomNumber());
	        student.setEmail(getRandomString());
	        student.setGpa(getRandomGPA());
	        student.setNumberOfUnits(getRandomUnits());
	
	    }catch (Exception e){
	        System.out.print(e.getMessage());
	    }
	
	    return student;
	}
	
	private String getRandomString(){
		String inputSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder nameBuilder = new StringBuilder();
		Random rnd = new Random();
		while (nameBuilder.length() < 6) { // length of the random string.
			int index = (int) (rnd.nextFloat() * inputSet.length());
			nameBuilder.append(inputSet.charAt(index));
		}
		String name = nameBuilder.toString();
		return name;
	}
	
	private double getRandomGPA(){
	    return 1.0 + Math.random() * (4 - 1);
	}
	
	private int getRandomNumber(){
	    return (int)(800000000 + Math.random() * (900000000 - 800000000));
	}
	
	private int getRandomUnits(){
		return (int)(0 + Math.random() * (150 - 0));
	   
	}

}
