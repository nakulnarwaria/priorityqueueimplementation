

import java.util.ArrayList;

import priorityQueue.PriorityHeap;
import java.util.Random;

import commandPattern.CommandInvoker;
import commandPattern.command.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.jupiter.api.Test;

public class CommandTest {
	
	@Test
	void testUndoAndRedoAllCommand() {
		
		PriorityHeap<String> queue = new PriorityHeap();
        CommandInvoker<String> invoker = new CommandInvoker(queue);
        
        for(int i=0;i<10;i++) 
        	invoker.execute(new AddElements(getRandomString()));
        
        
        for(int i=0;i<10;i++) 
        	invoker.undoCommand();
        assertEquals(0, queue.size());
        
        for(int i=0;i<10;i++) 
        	invoker.redoCommand();
        
        assertEquals(10, queue.size());
      	
	}
	
    @Test
    void testUndoRedoOnZeroCommands(){
    	
    	PriorityHeap<String> queue = new PriorityHeap();
        CommandInvoker<String> invoker = new CommandInvoker(queue);

        assertFalse(invoker.undoCommand());
        assertFalse(invoker.redoCommand());

    }
    
	@Test
    void testMixedCommands() {

        PriorityHeap<String> queue = new PriorityHeap();
        CommandInvoker<String> invoker = new CommandInvoker(queue);
        
        ArrayList<String> listOfRandomStrings = new ArrayList<String>();
        for(int i=0;i<10;i++)
        	listOfRandomStrings.add(getRandomString());
        
        invoker.execute(new AddElements(listOfRandomStrings));
        String existingString = queue.peek();

        invoker.execute(new RemoveElements<String>(existingString));
        assertEquals(9,queue.size());

        invoker.undoCommand();
        assertEquals(10,queue.size());

        
        for(int i=0;i<20;i++)
        	invoker.execute(new AddElements(getRandomString()));
        
        assertEquals(queue.size(),30);
        
        for(int i=0;i<5;i++)
        	invoker.undoCommand();
        
        assertEquals(queue.size(),25);
        
        for(int i=0;i<20;i++)
        	invoker.execute(new AddElements(getRandomString()));
        
        assertEquals(queue.size(),45);
        
        for(int i=0;i<25;i++) 
        	invoker.undoCommand();
        
        assertEquals(20, queue.size());
        
        for(int i=0;i<10;i++) 
        	invoker.redoCommand();

        invoker.execute(new RemoveElements<String>(listOfRandomStrings));
        assertEquals(20, queue.size());

        invoker.undoCommand();
        assertEquals(30, queue.size());

    }
    
    private String getRandomString(){
		String inputSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder nameBuilder = new StringBuilder();
		Random rnd = new Random();
		while (nameBuilder.length() < 6) { // length of the random string.
			int index = (int) (rnd.nextFloat() * inputSet.length());
			nameBuilder.append(inputSet.charAt(index));
		}
		String name = nameBuilder.toString();
		return name;
	}
  
}
