package commandPattern.command;

import java.util.ArrayList;
import java.util.List;
import priorityQueue.PriorityHeap;

public class AddElements<E> implements ICommand{

	private List<E> elements;
	
	public AddElements(List<E> elements) {
		this.elements =  elements;
	}
	
	public AddElements(E element) {
		this.elements = new ArrayList<E>();
		this.elements.add(element);
	}


	/*
	 * Add elements from elements queue to the priority queue.
	 */
	@Override
	public void execute(PriorityHeap queue) {

		for(E element:elements)	
			queue.add(element);	

	}

	/*
	 * Remove elements from priority queue that were added via this command.
	 */
	@Override
	public void unexecute(PriorityHeap queue) {
		
		for(E element:elements) 
			queue.remove(element);
		
	}

}
