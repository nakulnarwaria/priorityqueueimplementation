package commandPattern.command;

import priorityQueue.PriorityHeap;

/*
 * Command interface that need to be implemented by add command and remove command.
 */
public interface ICommand<E> {

	void execute(PriorityHeap<E> queue);
	void unexecute(PriorityHeap<E> queue);
	
}

