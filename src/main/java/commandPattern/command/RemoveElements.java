package commandPattern.command;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import priorityQueue.PriorityHeap;

public class RemoveElements<E> implements ICommand{

	private List<E> elements;
	
	public RemoveElements(List<E> elements) {
		this.elements = elements;
	}


	public RemoveElements(E element) {
		this.elements = new ArrayList<E>();
		this.elements.add(element);
	}

	/*
	 * Remove elements in elements list if it exist in queue.
	 */
	@Override
	public void execute(PriorityHeap queue) {
		
		List<E> elementsPresentInQueue = new ArrayList<E>();
		
		Iterator<E> elementIterator = elements.iterator();
		while (elementIterator.hasNext()) {
		    E element = elementIterator.next();
		    if (queue.remove(element))
		    	elementsPresentInQueue.add(element);
		}
		
		this.elements = elementsPresentInQueue;
		
	}
 	/*
 	 * Add back the elements that were removed from the queue via this command.
 	 */
	@Override
	public void unexecute(PriorityHeap queue) {
		for(E element:elements)	
			queue.add(element);		
	}

}
