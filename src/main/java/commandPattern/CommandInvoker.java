package commandPattern;

import priorityQueue.PriorityHeap;
import java.util.Arrays;
import java.util.List;
import commandPattern.command.*;

public class CommandInvoker<E> {
	
	private final int DEFAULT_CAPACITY=10; 
	private int totalCommands;
	ICommand<E>[] commandList;
	int capacity;
	PriorityHeap<E> queue;
	int executedCommandsIndex;


	/*
	 * Makes sure new command invoked via invoker are all stored in commandList by growing it's size.
	 */
	private void ensureExtraCapacity(){
        if(executedCommandsIndex==capacity-1){
            commandList = Arrays.copyOf(commandList, capacity*2);
            capacity*=2;
        }
    }
	
	public CommandInvoker(PriorityHeap<E> queue) {
		this.queue = queue;
		this.capacity = DEFAULT_CAPACITY;
		commandList = new ICommand[capacity];
		executedCommandsIndex=-1;
		totalCommands = -1;
	}

	/*
	 * Execute new command and add it to history of commands for later use.
	 */
	public void execute(ICommand<E> command) {
		
		ensureExtraCapacity();
		executedCommandsIndex++;
		totalCommands = executedCommandsIndex+1;
		commandList[executedCommandsIndex] = command;
		commandList[executedCommandsIndex].execute(queue);
	}

	/*
	 * Undo last executed command.
	 */
	public boolean undoCommand() {
		if(executedCommandsIndex == -1)
			return false;
		commandList[executedCommandsIndex].unexecute(queue);
		executedCommandsIndex--;
		return true;
	}

	/*
	 * Redo last undid command.
	 */
	public boolean redoCommand() {
		if(executedCommandsIndex == totalCommands-1 || totalCommands<0)
			return false;
		executedCommandsIndex++;
		commandList[executedCommandsIndex].execute(queue);
		return true;
	}
}
