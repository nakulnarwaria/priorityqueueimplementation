

public class ApplicationConstants {

	
	/*
	 * Custom Exception Messages.
	 */
	public static final String INVALID_INPUT = "Error receiving input - ";
	public static final String UNITS_RANGE_MSG = "Units should be between 0 to 150";
	public static final String GPA_RANGE_MSG = "Gpa should be between 0.0 to 4.0";
}
