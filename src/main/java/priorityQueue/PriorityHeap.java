package priorityQueue;

import java.io.Serializable;
import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.SortedSet;

/*
 * Max Priority Heap Implementation for student objects
 */
public class PriorityHeap<E> extends  AbstractQueue<E> implements Serializable{

	private final static int DEFAULT_INITIAL_CAPACITY = 50;
    private int capacity;
    private int size;
    private Object[] queue;
    private final Comparator<? super E> comparator;
    private int operationCount=0;

    
    public PriorityHeap(){
    	this(DEFAULT_INITIAL_CAPACITY, null);
    }
    
    public PriorityHeap(int initialCapacity) {
        this(initialCapacity, null);
    }
    
    public PriorityHeap(Comparator<? super E> comparator) {
        this(DEFAULT_INITIAL_CAPACITY, comparator);
    }
    
    public PriorityHeap(int initialCapacity, Comparator<? super E> comparator) {
		
    	if (initialCapacity < 1)
			throw new IllegalArgumentException();
		
    	this.capacity=initialCapacity;
	    this.queue = new Object[capacity];
		this.comparator = comparator;
	}
    
    @SuppressWarnings("unchecked")
	public PriorityHeap(Collection<? extends E> collection) {
        if (collection instanceof SortedSet<?>) {
            SortedSet<? extends E> set = (SortedSet<? extends E>) collection;
            this.comparator = (Comparator<? super E>) set.comparator();
            addElementsFromCollection(set);
        }
        else if (collection instanceof java.util.PriorityQueue<?>) {
            java.util.PriorityQueue<? extends E> queue = (java.util.PriorityQueue<? extends E>) collection;
            this.comparator = (Comparator<? super E>) queue.comparator();
            addElementsFromPriorityQueue(queue);
        }
        else {
            this.comparator = null;
            addElementsFromCollection(collection);
        }
    }
    
    @SuppressWarnings("unchecked")
	public PriorityHeap(java.util.PriorityQueue<? extends E> queue) {
        this.comparator = (Comparator<? super E>) queue.comparator();
        addElementsFromPriorityQueue(queue);
    }
    
    @SuppressWarnings("unchecked")
	public PriorityHeap(SortedSet<? extends E> set) {
        this.comparator = (Comparator<? super E>) set.comparator();
        addElementsFromCollection(set);
    }
    
    private void addElementsFromPriorityQueue(java.util.PriorityQueue<? extends E> queue) {
        if (queue.getClass() == java.util.PriorityQueue.class) {
            this.queue = queue.toArray();
            this.size = queue.size();
        } else {
        	addElementsFromCollection(queue);
        }
    }

    private void addElementsFromCollection(Collection<? extends E> collection) {
        Object[] objectArray = collection.toArray();
        if (objectArray.getClass() != Object[].class)
        	objectArray = Arrays.copyOf(objectArray, objectArray.length, Object[].class);
        int len = objectArray.length;
        if (len == 1 || this.comparator != null)
            for (Object e : objectArray)
                if (e == null)
                    throw new NullPointerException();
        this.queue = objectArray;
        this.size = objectArray.length;
        stabilizeHeap();
    }

    
    
    private void stabilizeHeap() {
        int parent = (size >>> 1) - 1;
        if (comparator == null)
            for (; parent >= 0; parent--)
                restoreHeapStateDown(parent);
        else
            for (; parent >= 0; parent--)
                restoreHeapStateDownUsingComparator(parent);
    }
    
    /*
     * ensureExtraCapacity ensures that heap is scalable, i.e.. heap capacity will increase with need.
     */
    private void ensureExtraCapacity(){
    	
    	if(size==capacity){
            queue = Arrays.copyOf(queue, capacity*2);
            capacity*=2;
        }
    }

    /*
     * Returns the student object with highest priority without removing it from the heap
     */
    @SuppressWarnings("unchecked")
	public E peek(){
        
    	if(size==0) return null;
        return (E) queue[0];
    }

    /*
     * Returns the student object with highest priority and removes it from the heap, and re-stabilize the heap.
     */
    @SuppressWarnings("unchecked")
	public E poll(){
        
    	if(size==0) return null;
        
    	E maxPriorityObject = (E) queue[0]; 
    	remove(queue[0]);
    	
    	return maxPriorityObject;
    }
    
    /*
     * Adds a student to the heap.
     */
    public boolean add(E newElement){
    	return offer(newElement);
    }
    
    public boolean offer(E newElement) {
    	
    	if(newElement==null) throw new NullPointerException();
        
    	ensureExtraCapacity();
        
    	queue[size] = newElement;
        
    	if(comparator==null)
        	restoreHeapStateUp(size);
        else
        	restoreHeapStateUpUsingComparator(size);
        
    	size++;
    	operationCount++;
        return true;
    }

    /*
     * Removes given argument from heap if it exist.
     */
    public boolean remove(Object element) {
    	
    	int index = indexOf(element);
    	
    	if(index<0)
    		return false;
    	
    	queue[index] = queue[size-1];   
    	size--;
        
    	if(comparator==null)
        	restoreHeapStateDown(index);
        else
        	restoreHeapStateDownUsingComparator(index);
    	
    	operationCount++;
    	return true;
    }
    
    private void swap(int indexOne, int indexTwo) {
    	
    	Object swapVariable  = queue[indexOne];
        queue[indexOne] = queue[indexTwo];
        queue[indexTwo] = swapVariable;
    }
        
    /*
     * Takes the last entered element in the heap, and swap it with it's parent if priority is higher until heap is stable. 
     * Methods for cases when comparator is provided and not provided are implemented below.
     */
    @SuppressWarnings("unchecked")
	private void restoreHeapStateUpUsingComparator(int index){
        
    	while((index-1)/2>=0 && comparator.compare((E)queue[(index-1)/2],(E)queue[index])>0){
            swap((index-1)/2,index);
            index = (index-1)/2;
        }
    }
    
    
    @SuppressWarnings("unchecked")
	private void restoreHeapStateUp(int index){
        
    	while((index-1)/2>=0 && ((Comparable<? super E>) queue[(index-1)/2]).compareTo((E) queue[index]) > 0) {    
    		swap(index, (index-1)/2);
            index = (index-1)/2;
    	}
    }

    /*
     * Takes the top element and swap it with appropriate child if it's priority is lower than it's children until heap is stable.
     * Methods for cases when comparator is provided and not provided are implemented below. 
     */
    @SuppressWarnings("unchecked")
	private void restoreHeapStateDownUsingComparator(int index){
        
    	while(2*(index)+1<size){
            
        	int largerPriorityIndex = 2*(index)+1;
            
            if( 2*(index)+2<size && comparator.compare((E)queue[2*(index)+2],(E)queue[2*index+1])<0)
                largerPriorityIndex = 2*(index)+2;
            
            if(comparator.compare((E)queue[index],(E)queue[largerPriorityIndex])<0)
                break;
            else 
            	swap(largerPriorityIndex,index);
            
            index = largerPriorityIndex;
        }
    }

    @SuppressWarnings("unchecked")
	private void restoreHeapStateDown(int index){
        
    	while(2*(index)+1<size){
            
    		int largerPriorityIndex = 2*(index)+1;
            
    		if( 2*(index)+2<size && ((Comparable<? super E>) queue[2*(index)+2]).compareTo((E) queue[2*index+1]) < 0)
                largerPriorityIndex = 2*(index)+2;
            
    		if(((Comparable<? super E>) queue[index]).compareTo((E) queue[largerPriorityIndex]) < 0)
                break;
            else 
            	swap(largerPriorityIndex,index);
            
    		index = largerPriorityIndex;
        }
    }
    
    /*
     * Returns index of argument element in the queue if it exist, else return -1.
     */
    private int indexOf(Object element) {
        
    	if (element != null) {
            for (int i = 0; i < size; i++)
                if (element.equals(queue[i]))
                    return i;
        }
        
    	return -1;
    }
    
    /*
     * Returns list of all the students in the heap, sorted by priority.
     */
    public List<E> getPrioritySortedList(){
        
    	ArrayList<E> returnList = new ArrayList<E>();
    	Object[] currentHeapCopy = new Object[queue.length];
        int sizeCopy = size;
        
        for(int i=0;i<size;i++)
            currentHeapCopy[i] = queue[i];
        

        for(int i=0;i<sizeCopy;i++) {
        	E nextPriorityObject = this.poll();
        	returnList.add(nextPriorityObject);
        }
            
        for(int i=0;i<sizeCopy;i++)
            queue[i] = currentHeapCopy[i];
        
        this.size = sizeCopy;
        return returnList;
    }
    
    public int size() {
    	return size;
    }

    /*
     * Returns copy of internal array in this class.
     */
    public Object[] toArray() {
    	return Arrays.copyOf(queue, size);
    }
    
    public <T> T[] toArray(T[] array) {
        
    	final int size = this.size;
        
    	if (array.length < size)
            return (T[]) Arrays.copyOf(queue, size, array.getClass());
        
    	System.arraycopy(queue, 0, array, 0, size);
        
    	if (array.length > size)
            array[size] = null;
        
    	return array;
    }
    
    public String toString() {
    	return Arrays.toString(this.toArray());
    }
    
    public boolean contains(Object element) {
        return indexOf(element) >= 0;
    }
    
    public Comparator<? super E> comparator() {
        return comparator;
    }
    
    /*
     * Empty the queue.
     */
    public void clear() {
    	this.size=0;
    	this.queue = new Object[capacity];
    }

    /*
     * Returns iterator for this priority queue.
     */
	@Override
	public Iterator<E> iterator() {
		return new Itr<E>();
	}
	
	private final class Itr<E> implements Iterator<E>{

        int cursor = 0;
        private int expectedChangeCounter = operationCount;

        @Override
        public boolean hasNext() {
            return cursor < size;
        }

        @Override
        public E next() {
            if (expectedChangeCounter != operationCount)
                throw new ConcurrentModificationException();
            if (cursor < size)
                return (E) queue[cursor++];
            throw new NoSuchElementException();
        }

    }
    
}
