
class Student implements Comparable<Student>{
	
	private String name;
    private int redId;
    private String email;
    private String address;
    private double gpa=0;
    private int numberOfUnits=0;


    /*
     * Getter and Setter methods for class variables.
     */
    public String getName() {
    	return name;
    }
	
    public int getRedId() {
		return redId;
	}

	public void setRedId(int redId) {
		this.redId = redId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getGpa() {
		return gpa;
	}

	public void setGpa(double gpa) throws Exception {
		if(gpa>=0 && gpa<=4)
			this.gpa = gpa;
		else
            throw new Exception(ApplicationConstants.INVALID_INPUT+ " " + ApplicationConstants.GPA_RANGE_MSG);
	}

	public int getNumberOfUnits() {
		return numberOfUnits;
	}

	public void setNumberOfUnits(int numberOfUnits) throws Exception {
		if(numberOfUnits>=0 && numberOfUnits<=150)
			this.numberOfUnits = numberOfUnits;
		else
            throw new Exception(ApplicationConstants.INVALID_INPUT + " " + ApplicationConstants.UNITS_RANGE_MSG);

	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	/*
	 * Calculate priority with 70% weightage of number of units taken by the student, and 30% weightage of student's GPA
	 */
    private double getPriority(){
	    double gpaToPercent = (gpa/4)*100; //normalize GPA to scale of 100
	    double numberOfUnitsToPercent = (numberOfUnits/150)*100; // normalize number of units taken to scale of 100
	    double priority = numberOfUnitsToPercent*0.7 + gpaToPercent*0.3; // normalize number of units taken to scale of 70, normalize GPA to scale of 30, and add both
	    return priority;
    }
    
    @Override
    public int compareTo(Student student) {
        if (this.getPriority() > student.getPriority()) return -1;
        if (this.getPriority() < student.getPriority()) return 1;
        return 0;
    }
    
    
}
